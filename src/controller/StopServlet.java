package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.UserManageService;

@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	int isStop = 1;
    	int userId = Integer.parseInt(request.getParameter("userId"));

    	new UserManageService().isStop(isStop,userId);

    	response.sendRedirect("usermanage");
    }

}