package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


    	User user = (User) request.getSession().getAttribute("loginUser");

    	String category = request.getParameter("category");
    	String minDate = request.getParameter("minDate");
    	String maxDate = request.getParameter("maxDate");

    	List<UserMessage> messages = new MessageService().getMessage(category, minDate, maxDate);
    	request.setAttribute("messages", messages);

    	List<UserComment> comments = new CommentService().getComment();
    	request.setAttribute("comments", comments);

    	request.setAttribute("category", category);
    	request.setAttribute("minDate", minDate);
    	request.setAttribute("maxDate", maxDate);

    	request.getRequestDispatcher("/top.jsp").forward(request, response);
    }


}