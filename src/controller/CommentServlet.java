package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import beans.Comment;
import beans.User;
import service.CommentService;
import service.GetCommentId;


@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException{

		request.getRequestDispatcher("top.jsp").forward(request, response);

	}

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();
        Comment comment = createCommentData(request);
        List<String> messages = new ArrayList<String>();

		boolean isSuccess = false;
		String responseJson = "";
		String errors = "";

        if (isValid(comment, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            comment.setLoginId(user.getLoginId());
            new CommentService().register(comment);
            Comment commentId = new GetCommentId().getId();
            int id = commentId.getId();
            isSuccess = true;
			responseJson = String.format("{\"is_success\" : \"%s\", \"id\" : %s}", isSuccess, id);


        } else {
        	errors = new ObjectMapper().writeValueAsString(messages);
			responseJson = String.format("{\"is_success\" : \"%s\", \"errors\" : %s}", isSuccess, errors);

        }

		response.setContentType("application/json;charset=UTF-8");
		response.getWriter().write(responseJson);
    }

	/*
	 * Json文字列をJavaオブジェクトに変換するメソッド
	 */
	private Comment createCommentData(HttpServletRequest request) {
		String data = request.getParameter("comment");

		Comment comment = new Comment();
		try {
			comment = new ObjectMapper().readValue(data, Comment.class);
		} catch(Exception e) {
			e.printStackTrace();
		}

		return comment;
	}

    private boolean isValid(Comment comment, List<String> messages) {

    	String text = comment.getText();

        if (StringUtils.isBlank(text)) {
            messages.add("コメントを入力してください");
        } else if(text.length() > 500) {
			messages.add("コメントは500文字以下で入力してください");
		}

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}