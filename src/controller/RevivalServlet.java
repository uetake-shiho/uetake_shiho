package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.UserManageService;

@WebServlet(urlPatterns = { "/revival" })
public class RevivalServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


    	int isRevival = 0;
    	int userId = Integer.parseInt(request.getParameter("userId"));

    	new UserManageService().isRevival(isRevival, userId);

    	response.sendRedirect("usermanage");
    }

}