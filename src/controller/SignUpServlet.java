package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.BranchId;
import beans.PositionId;
import beans.User;
import service.BranchIdService;
import service.PositionIdService;
import service.UserService;

@WebServlet(urlPatterns = {"/signup"})

public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException{

        List<BranchId> branches = new BranchIdService().getBranch();
        request.setAttribute("branches", branches);

        List<PositionId> positions = new PositionIdService().getPosition();
        request.setAttribute("positions", positions);

		request.getRequestDispatcher("signup.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException{

		List<String>messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User signUpUser = getSignUpUser(request);
		List<BranchId> branches = new BranchIdService().getBranch();
		List<PositionId> positions = new PositionIdService().getPosition();


		if(isValid(request, messages) == true){

			new UserService().register(signUpUser);

			response.sendRedirect("usermanage");
			return;

		}else {

			session.setAttribute("errorMessages", messages);
            request.setAttribute("signUpUser", signUpUser);
            request.setAttribute("branches", branches);
            request.setAttribute("positions", positions);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	 private User getSignUpUser(HttpServletRequest request)
	            throws IOException, ServletException {

	        User signUpUser = new User();
	        signUpUser.setName(request.getParameter("name"));
	        signUpUser.setLoginId(request.getParameter("loginId"));
	        signUpUser.setPassword(request.getParameter("password"));
	        signUpUser.setBranchId(request.getParameter("branchId"));
	        signUpUser.setPositionId(request.getParameter("positionId"));
	        return signUpUser;
	    }

	private boolean isValid(HttpServletRequest request, List<String>messages) {
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		String name = request.getParameter("name");
		int branchId = Integer.parseInt(request.getParameter("branchId"));
		int positionId = Integer.parseInt(request.getParameter("positionId"));

		if(StringUtils.isBlank(loginId)) {
			messages.add("ログインIDを入力してください");
		} else {
			User user = new UserService().check(loginId);
			if(user != null) {
				messages.add("既に使用されているログインIDです");
			}else if(loginId.matches("^[0-9a-zA-Z]{6,20}$") == false) {
				messages.add("ログインIDは半角英数字6文字以上20文字以下で入力してください");
			}
		}

		if(StringUtils.isBlank(name)) {
			messages.add("ユーザー名を入力してください");
		} else if(name.length() > 10) {
			messages.add("ユーザー名は10文字以下で入力してください");
		}

		if(StringUtils.isBlank(password)) {
			messages.add("パスワードを入力してください");
		}else if(password.equals(passwordConfirm) == false){
			messages.add("入力されたパスワードと確認用パスワードが一致しません");
		}else if(password.matches("^[-@+*;:#$%&0-9a-zA-Z]{6,20}$") == false) {
			messages.add("パスワードは半角文字6文字以上20文字以下で入力してください");
		}

		if(branchId == 1) {
			if(positionId != 1 && positionId != 2) {
				messages.add("支店と役職の組み合わせが無効です");
			}
		}else{
			if(positionId == 1 || positionId == 2){
			messages.add("支店と役職の組み合わせが無効です");
			}
		}

		if(messages.size() == 0) {
			return true;
		}else {
			return false;
		}
	}

}
