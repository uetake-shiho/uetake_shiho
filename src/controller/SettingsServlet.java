package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.BranchId;
import beans.PositionId;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchIdService;
import service.PositionIdService;
import service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    	String userId = request.getParameter("userId");
        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();

    	if(userId == null) {
    		response.sendRedirect("usermanage");
    		return;
    	}else if(userId.matches("^[0-9]*$") == false){
	    	messages.add("存在しないユーザーです");
	    	session.setAttribute("errorMessages", messages);
	    	response.sendRedirect("usermanage");
	    	return;
    	}else {
		   	User editUser = new UserService().getUser(userId);
		   	if(editUser == null) {
		    	messages.add("存在しないユーザーです");
		    	session.setAttribute("errorMessages", messages);
		    	response.sendRedirect("usermanage");
		    	return;
		    }

		   	request.setAttribute("editUser", editUser);

		   	List<BranchId> branches = new BranchIdService().getBranch();
		   	request.setAttribute("branches", branches);

		   	List<PositionId> positions = new PositionIdService().getPosition();
		   	request.setAttribute("positions", positions);

		   	request.getRequestDispatcher("settings.jsp").forward(request, response);
    	}

    }


    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        User editUser = getEditUser(request);

        List<BranchId> branches = new BranchIdService().getBranch();
        List<PositionId> positions = new PositionIdService().getPosition();

        if (isValid(request, messages) == true) {

            try {
                new UserService().update(editUser);
            } catch (NoRowsUpdatedRuntimeException e) {
                messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
                session.setAttribute("errorMessages", messages);
                request.setAttribute("editUser", editUser);
                request.setAttribute("branches", branches);
                request.setAttribute("positions", positions);
                request.getRequestDispatcher("settings.jsp").forward(request, response);
                return;
            }
            response.sendRedirect("usermanage");

        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);
            request.setAttribute("branches", branches);
            request.setAttribute("positions", positions);
            request.getRequestDispatcher("settings.jsp").forward(request, response);
        }
    }

    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setName(request.getParameter("name"));
        editUser.setLoginId(request.getParameter("loginId"));
        editUser.setPassword(request.getParameter("password"));
        editUser.setBranchId(request.getParameter("branchId"));
        editUser.setPositionId(request.getParameter("positionId"));
        return editUser;
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	int settingId = Integer.parseInt(request.getParameter("id"));
        String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		String name = request.getParameter("name");
		int branchId = Integer.parseInt(request.getParameter("branchId"));
		int positionId = Integer.parseInt(request.getParameter("positionId"));

        if (StringUtils.isBlank(loginId)) {
            messages.add("ログインIDを入力してください");
        }else if(loginId.matches("^[0-9a-zA-Z]{6,20}$") == false) {
			messages.add("ログインIDは半角英数字6文字以上20文字以下で入力してください");
		}else{
			User user = new UserService().check(loginId);
			if(user != null) {
				int id = user.getId();
				if(id != settingId) {
					messages.add("既に使用されているログインIDです");
				}
			}
		}


        if (StringUtils.isBlank(name)) {
            messages.add("ユーザー名を入力してください");
        }else if(name.length() > 10) {
			messages.add("ユーザー名は10文字以下で入力してください");
		}

        if (StringUtils.isNotEmpty(password)) {
        	if(password.matches("^[-@+*;:#$%&0-9a-zA-Z]{6,20}$") == false) {
        		messages.add("パスワードは半角文字6文字以上20文字以下で入力してください");
        	}
        	if(password.equals(passwordConfirm) == false){
        		messages.add("入力されたパスワードと確認用パスワードが一致しません");
        	}
		}


		if(branchId == 1) {
			if(positionId != 1 && positionId != 2) {
				messages.add("支店と役職の組み合わせが無効です");
			}
		}else {
			if(positionId == 1 || positionId == 2){
				messages.add("支店と役職の組み合わせが無効です");
			}
		}

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}