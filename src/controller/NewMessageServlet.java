package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException{

		request.getRequestDispatcher("messages.jsp").forward(request, response);

	}

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	Message message = createMessageData(request);
        HttpSession session = request.getSession();

		boolean isSuccess = false;
		String responseJson = "";
		String errors = "";

        List<String> messages = new ArrayList<String>();

        if (isValid(message, messages) == true) {

            User user = (User) session.getAttribute("loginUser");
            message.setLoginId(user.getLoginId());
            new MessageService().register(message);
            isSuccess = true;
			responseJson = String.format("{\"is_success\" : \"%s\"}", isSuccess);

			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().write(responseJson);


        } else {

        	errors = new ObjectMapper().writeValueAsString(messages);
			responseJson = String.format("{\"is_success\" : \"%s\", \"errors\" : %s}", isSuccess, errors);


			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().write(responseJson);
        }

    }

	/*
	 * Json文字列をJavaオブジェクトに変換するメソッド
	 */
	private Message createMessageData(HttpServletRequest request) {
		String data = request.getParameter("message");

		Message message = new Message();
		try {
			message = new ObjectMapper().readValue(data, Message.class);
		} catch(Exception e) {
			e.printStackTrace();
		}

		return message;
	}

    private boolean isValid(Message message, List<String> messages) {

        String subject = message.getSubject();
        String text = message.getText();
        String category = message.getCategory();

        if (StringUtils.isBlank(subject)) {
            messages.add("件名を入力してください");
        } else if(subject.length() > 30) {
			messages.add("件名は30文字以下で入力してください");
		}

        if (StringUtils.isBlank(text)) {
            messages.add("本文を入力してください");
        } else if(text.length() > 1000) {
			messages.add("本文は1000文字以下で入力してください");
		}

        if (StringUtils.isBlank(category)) {
            messages.add("カテゴリーを入力してください");
        } else if(category.length() > 10) {
			messages.add("カテゴリーは10文字以下で入力してください");
		}

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}