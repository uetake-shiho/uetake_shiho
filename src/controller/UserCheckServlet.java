package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import beans.User;
import service.UserService;


@WebServlet(urlPatterns = { "/usercheck" })
public class UserCheckServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException{

		request.getRequestDispatcher("top.jsp").forward(request, response);

	}

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User userCheck = userCheck(request);
        List<String> messages = new ArrayList<String>();

		boolean isSuccess = false;
		String responseJson = "";
		String errors = "";

        if (isValid(userCheck, messages) == true) {

            isSuccess = true;
			responseJson = String.format("{\"is_success\" : \"%s\"}", isSuccess);

        } else {
        	errors = new ObjectMapper().writeValueAsString(messages);
			responseJson = String.format("{\"is_success\" : \"%s\", \"errors\" : %s}", isSuccess, errors);

        }

		response.setContentType("application/json;charset=UTF-8");
		response.getWriter().write(responseJson);
    }

	/*
	 * Json文字列をJavaオブジェクトに変換するメソッド
	 */
	private User userCheck(HttpServletRequest request) {
		String data = request.getParameter("userId");

		User userCheck = new User();
		try {
			userCheck = new ObjectMapper().readValue(data, User.class);
		} catch(Exception e) {
			e.printStackTrace();
		}

		return userCheck;
	}

    private boolean isValid(User userCheck, List<String> messages) {

    	String checkId = userCheck.getLoginId();
    	System.out.println(checkId);

		if(StringUtils.isBlank(checkId)) {
			messages.add("ログインIDを入力してください");
		} else {
			User user = new UserService().check(checkId);
			if(user != null) {
				messages.add("既に使用されているログインIDです");
			}else if(checkId.matches("^[0-9a-zA-Z]{6,20}$") == false) {
				messages.add("ログインIDは半角英数字6文字以上20文字以下で入力してください");
			}
		}
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}