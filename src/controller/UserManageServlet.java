package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.BranchId;
import beans.PositionId;
import beans.UserManage;
import service.BranchIdService;
import service.PositionIdService;
import service.UserManageService;


@WebServlet(urlPatterns = { "/usermanage" })
public class UserManageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<UserManage> users = new UserManageService().getManage();
        request.setAttribute("users", users);

        List<BranchId> branches = new BranchIdService().getBranch();
        request.setAttribute("branches", branches);

        List<PositionId> positions = new PositionIdService().getPosition();
        request.setAttribute("positions", positions);

    	request.getRequestDispatcher("/manage.jsp").forward(request, response);

    }
}