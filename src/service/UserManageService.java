package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.UserManage;
import dao.UserManageDao;

public class UserManageService {



	public List<UserManage> getManage() {

	    Connection connection = null;

	    try {
	        connection = getConnection();

	        UserManageDao messageDao = new UserManageDao();
	        List<UserManage> ret = messageDao.getUsers(connection);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}

	  public void isStop(int isStop, int userId) {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            UserManageDao usermanageDao = new UserManageDao();
	            usermanageDao.isStop(connection, isStop, userId);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }

	    public void isRevival(int isRevival, int userId) {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            UserManageDao usermanageDao = new UserManageDao();
	            usermanageDao.isRevival(connection, isRevival, userId);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }

}
