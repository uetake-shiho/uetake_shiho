package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.BranchId;
import dao.BranchIdDao;

public class BranchIdService {



	public List<BranchId> getBranch() {

	    Connection connection = null;

	    try {
	        connection = getConnection();

	        BranchIdDao branchDao = new BranchIdDao();
	        List<BranchId> ret = branchDao.getBranch(connection);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}

}
