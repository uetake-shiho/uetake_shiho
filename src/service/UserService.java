package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

import beans.User;
import dao.UserCheckDao;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {

	public void register(User user) {
		Connection connection = null;
		try {
			connection = getConnection();

			String enPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(enPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

    public User getUser(String userId) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            User user = userDao.getSettingUser(connection, userId);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            if(StringUtils.isEmpty(user.getPassword()) != true ) {
	            String encPassword = CipherUtil.encrypt(user.getPassword());
	            user.setPassword(encPassword);
            }

            UserDao userDao = new UserDao();
            userDao.update(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public User check(String loginId) {
		Connection connection = null;
		try {
			connection = getConnection();

			UserCheckDao userCheckDao = new UserCheckDao();
			User user = userCheckDao.check(connection, loginId);

			commit(connection);

			return user;

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}




}
