package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

    public void register(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    public List<UserMessage> getMessage(String category, String minDate, String maxDate){

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		String startAt = "2018-12-01";
    		String endAt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Timestamp(System.currentTimeMillis()));


    		if(StringUtils.isEmpty(minDate) && StringUtils.isEmpty(maxDate)) {

    			UserMessageDao messageDao = new UserMessageDao();
    			List<UserMessage> ret = messageDao.getUserMessages(connection,  category, startAt, endAt);

        		commit(connection);
        		return ret;

    		}else if(StringUtils.isNotEmpty(minDate) && StringUtils.isEmpty(maxDate)){

    			UserMessageDao messageDao = new UserMessageDao();
    			List<UserMessage> ret = messageDao.getUserMessages(connection, category, minDate, endAt);

        		commit(connection);
        		return ret;

    		}else if(StringUtils.isEmpty(minDate) && StringUtils.isNotEmpty(maxDate)) {

    			String max = maxDate + " 23:59:59";
    			UserMessageDao messageDao = new UserMessageDao();
    			List<UserMessage> ret = messageDao.getUserMessages(connection,  category, startAt, max);

        		commit(connection);
        		return ret;

    		}else{

    			String max = maxDate + " 23:59:59";
    			UserMessageDao messageDao = new UserMessageDao();
    			List<UserMessage> ret = messageDao.getUserMessages(connection, category, minDate, max);

        		commit(connection);
        		return ret;

    		}


    	}catch(RuntimeException e) {
    		rollback(connection);
    		throw e;
    	}catch(Error e) {
    		rollback(connection);
    		throw e;
    	}finally {
    		close(connection);
    	}
    }

    public void delete(int messageId) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.delete(connection, messageId);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


}