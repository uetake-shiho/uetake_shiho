package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.PositionId;
import dao.PositionIdDao;

public class PositionIdService {



	public List<PositionId> getPosition(){

	    Connection connection = null;

	    try {
	        connection = getConnection();

	        PositionIdDao positionDao = new PositionIdDao();
	        List<PositionId> ret = positionDao.getPosition(connection);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}

}
