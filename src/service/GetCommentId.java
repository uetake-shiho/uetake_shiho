package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.Comment;
import dao.GetCommentIdDao;

public class GetCommentId {

	public Comment getId() {

		Connection connection = null;
		try {
			connection = getConnection();

			GetCommentIdDao getCommentIdDao = new GetCommentIdDao();
			Comment comment = getCommentIdDao.getId(connection);


			commit(connection);

			return comment;

		}catch (RuntimeException e) {
			rollback(connection);
			throw e;
		}catch (Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);

		}
	}

}
