package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns = {"/usermanage","/signup","/settings","/manage.jsp","/signup.jsp","/settings.jsp"})
public class AuthorityFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		try {
			HttpSession session = ((HttpServletRequest)request).getSession();
			User loginUser = (User)session.getAttribute("loginUser");
			List<String>messages = new ArrayList<String>();

			if(loginUser == null) {

				chain.doFilter(request, response);

			}else {

				int branchId = Integer.parseInt(loginUser.getBranchId());
				int positionId = Integer.parseInt(loginUser.getPositionId());

				if(branchId == 1 && positionId == 1) {

					chain.doFilter(request, response);

				}else {

					messages.add("管理者権限がありません");
					session.setAttribute("errorMessages", messages);
					((HttpServletResponse)response).sendRedirect("./");
					return;
				}
			}

		}catch(ServletException se) {
		}catch(IOException e) {
		}

	}

	public void init(FilterConfig filterConfig)throws ServletException{

	}

    @Override
    public void destroy() {
    }
}