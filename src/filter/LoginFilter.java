package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		try {

			String target = ((HttpServletRequest)request).getRequestURI();
			HttpSession session = ((HttpServletRequest)request).getSession();
			String servletPath = ((HttpServletRequest)request).getServletPath();

			User loginCheck = (User)session.getAttribute("loginUser");
			List<String>messages = new ArrayList<String>();


			if(!(target.equals("/Uetake_Shiho/css/style.css")) && !(servletPath.equals("/login")) && !(target.equals("/Uetake_Shiho/js/ajax.js"))) {

				if(loginCheck == null) {

					session.setAttribute("target", target);
					messages.add("ログインしてください");
					session.setAttribute("errorMessages", messages);
					((HttpServletResponse)response).sendRedirect("login");
					return;

				}else {

					chain.doFilter(request, response);
				}

			}else if(servletPath.equals("/login")) {
				session = ((HttpServletRequest)request).getSession(true);
				chain.doFilter(request, response);

			}else if(target.equals("/Uetake_Shiho/css/style.css") || (target.equals("/Uetake_Shiho/js/ajax.js"))) {
				chain.doFilter(request, response);
			}


		}catch(ServletException se) {
		}catch(IOException e) {
		}

	}

	public void init(FilterConfig filterConfig)throws ServletException{

	}

    @Override
    public void destroy() {
    }
}