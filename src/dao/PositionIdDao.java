package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.PositionId;
import exception.SQLRuntimeException;

public class PositionIdDao {

	public List<PositionId> getPosition(Connection connection){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("id,");
			sql.append("name  ");
			sql.append("FROM positions;  ");

			ps = connection.prepareStatement(sql.toString());


			ResultSet rs = ps.executeQuery();

			List<PositionId> ret = toPositionIdList(rs);
			return ret;


		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	private List<PositionId> toPositionIdList(ResultSet rs) throws SQLException{

		List<PositionId> ret = new ArrayList<PositionId>();

		try {
			while(rs.next()) {
				String name = rs.getString("name");
				int id = rs.getInt("id");


				PositionId positions = new PositionId();
				positions.setName(name);
				positions.setId(id);


				ret.add(positions);
			}
			return ret;
		}finally{

			close(rs);
		}
	}

}