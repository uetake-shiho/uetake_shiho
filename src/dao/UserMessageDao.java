package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> getUserMessages(Connection connection,String category, String startAt, String endAt){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id,  ");
			sql.append("messages.subject,  ");
			sql.append("messages.text,  ");
			sql.append("messages.category,  ");
			sql.append("messages.login_id,  ");
			sql.append("users.name,  ");
			sql.append("messages.created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.login_id = users.login_id ");
            sql.append("WHERE messages.created_date BETWEEN '"+ startAt + "' ");
            sql.append("AND '" + endAt + "' ");
			if(StringUtils.isNotEmpty(category)) {
				sql.append("AND category LIKE '%" + category + "%' ");
			}
			sql.append("ORDER BY created_date DESC ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserMessage> ret = toUserMessageList(rs);
			return ret;


		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessageList(ResultSet rs) throws SQLException{

		List<UserMessage> ret = new ArrayList<UserMessage>();

		try {
			while(rs.next()) {
				String name = rs.getString("name");
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String subject = rs.getString("subject");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserMessage message = new UserMessage();
				message.setName(name);
				message.setId(id);
				message.setLoginId(loginId);
				message.setSubject(subject);
				message.setText(text);
				message.setCategory(category);
				message.setCreated_date(createdDate);

				ret.add(message);
			}
			return ret;
		}finally{

			close(rs);
		}
	}

}
