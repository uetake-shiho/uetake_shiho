package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import exception.SQLRuntimeException;

public class GetCommentIdDao {

	public Comment getId(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * FROM comments ORDER BY id DESC LIMIT 1 ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();

            List<Comment>commentList = toCommentList(rs);

			if(commentList.isEmpty() == true) {
				return null;
			}else if(2 <= commentList.size()) {
				throw new IllegalStateException("2 <= commentList.size()");
			}else {
				return commentList.get(0);
			}

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
        	close(ps);
        }
    }

    private List<Comment> toCommentList(ResultSet rs) throws SQLException{

		List<Comment> ret = new ArrayList<Comment>();

		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				Comment comment = new Comment();
				comment.setId(id);

				ret.add(comment);
			}
			return ret;
		}finally{

			close(rs);
		}
	}

}
