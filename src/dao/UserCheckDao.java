package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;

public class UserCheckDao {

	public User check(Connection connection, String loginId){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users WHERE login_id = '" + loginId + "' ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<User> userCheckList = toUserCheckList(rs);

			if(userCheckList.isEmpty() == true) {
				return null;
			}else {
				return userCheckList.get(0);
			}

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	private List<User> toUserCheckList(ResultSet rs) throws SQLException{

		List<User> ret = new ArrayList<User>();

		try {
			while(rs.next()) {
				String name = rs.getString("name");
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String branchId = String.valueOf(rs.getInt("branch_id"));
				String positionId = String.valueOf(rs.getInt("position_id"));
				int isStopped = rs.getInt("is_stopped");
				Timestamp createdDate = rs.getTimestamp("created_date");

				User manage = new User();
				manage.setName(name);
				manage.setId(id);
				manage.setLoginId(loginId);
				manage.setBranchId(branchId);
				manage.setPositionId(positionId);
				manage.setIsStopped(isStopped);
				manage.setCreatedDate(createdDate);


				ret.add(manage);
			}
			return ret;
		}finally{

			close(rs);
		}
	}

}
