package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> getUserComments(Connection connection, int num){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id,  ");
			sql.append("comments.login_id,  ");
			sql.append("comments.text,  ");
			sql.append("comments.message_id,  ");
			sql.append("users.name,  ");
			sql.append("comments.created_date ");
			sql.append("FROM comments  ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.login_id = users.login_id ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserComment> ret = toUserCommentList(rs);
			return ret;


		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	private List<UserComment> toUserCommentList(ResultSet rs) throws SQLException{

		List<UserComment> ret = new ArrayList<UserComment>();

		try {
			while(rs.next()) {
				String name = rs.getString("name");
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				int messageId = rs.getInt("message_id");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserComment comment = new UserComment();
				comment.setName(name);
				comment.setId(id);
				comment.setLoginId(loginId);
				comment.setMessageId(messageId);
				comment.setText(text);
				comment.setCreated_date(createdDate);

				ret.add(comment);
			}
			return ret;
		}finally{

			close(rs);
		}
	}

}
