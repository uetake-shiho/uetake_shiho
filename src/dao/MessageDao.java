package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Message;
import exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("login_id");
            sql.append(",subject");
            sql.append(", text");
            sql.append(",category");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?");
            sql.append(",?");
            sql.append(",?");
            sql.append(",?");
            sql.append(",CURRENT_TIMESTAMP");
            sql.append(",CURRENT_TIMESTAMP");
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, message.getLoginId());
            ps.setString(2, message.getSubject());
            ps.setString(3, message.getText());
            ps.setString(4, message.getCategory());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void delete(Connection connection, int messageId) {

        PreparedStatement ps = null;
        try {
            String sql = "DELETE FROM messages WHERE id = ? ";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, messageId);
			ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}
