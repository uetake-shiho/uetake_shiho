package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.BranchId;
import exception.SQLRuntimeException;

public class BranchIdDao {

	public List<BranchId> getBranch(Connection connection){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("id,");
			sql.append("name  ");
			sql.append("FROM branches;  ");

			ps = connection.prepareStatement(sql.toString());


			ResultSet rs = ps.executeQuery();

			List<BranchId> ret = toBranchIdList(rs);
			return ret;


		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	private List<BranchId> toBranchIdList(ResultSet rs) throws SQLException{

		List<BranchId> ret = new ArrayList<BranchId>();

		try {
			while(rs.next()) {
				String name = rs.getString("name");
				int id = rs.getInt("id");

				BranchId branches = new BranchId();
				branches.setName(name);
				branches.setId(id);

				ret.add(branches);
			}
			return ret;
		}finally{

			close(rs);
		}
	}

}
