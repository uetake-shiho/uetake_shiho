package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserManage;
import exception.SQLRuntimeException;

public class UserManageDao {

	public List<UserManage> getUsers(Connection connection){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("id,");
			sql.append("login_id,  ");
			sql.append("password,  ");
			sql.append("name,  ");
			sql.append("branch_id,  ");
			sql.append("position_id,  ");
			sql.append("is_stopped, ");
			sql.append("created_date, ");
			sql.append("updated_date ");
			sql.append("FROM users;  ");

			ps = connection.prepareStatement(sql.toString());


			ResultSet rs = ps.executeQuery();

			List<UserManage> ret = toUserManageList(rs);
			return ret;


		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	private List<UserManage> toUserManageList(ResultSet rs) throws SQLException{

		List<UserManage> ret = new ArrayList<UserManage>();

		try {
			while(rs.next()) {
				String name = rs.getString("name");
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				int branchId = rs.getInt("branch_id");
				int positionId = rs.getInt("position_id");
				int isStopped = rs.getInt("is_stopped");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserManage manage = new UserManage();
				manage.setName(name);
				manage.setId(id);
				manage.setLoginId(loginId);
				manage.setBranch_id(branchId);
				manage.setPosition_id(positionId);
				manage.setIs_stopped(isStopped);
				manage.setCreated_date(createdDate);


				ret.add(manage);
			}
			return ret;
		}finally{

			close(rs);
		}
	}
    public void isStop(Connection connection, int isStop, int userId) {

        PreparedStatement ps = null;
        try {
        	StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append(" is_stopped = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());
 			ps.setInt(1, 1);
 			ps.setInt(2, userId);

 			ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void isRevival(Connection connection, int isRevival, int userId) {

    	 PreparedStatement ps = null;
         try {
         	StringBuilder sql = new StringBuilder();
             sql.append("UPDATE users SET");
             sql.append(" is_stopped = ?");
             sql.append(", updated_date = CURRENT_TIMESTAMP");
             sql.append(" WHERE");
             sql.append(" id = ?");

             ps = connection.prepareStatement(sql.toString());
  			ps.setInt(1, 0);
  			ps.setInt(2, userId);

  			ps.executeUpdate();


         } catch (SQLException e) {
             throw new SQLRuntimeException(e);
         } finally {
             close(ps);
         }
    }

}
