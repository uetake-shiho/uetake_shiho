<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3//DTD HTML 4.01 transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" charset="UTF-8"></script>
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="./js/ajax.js"></script>
	<title>ユーザー登録</title>
</head>
	<body>
		<div class="header">
			<div class="container">
				<div class="header-left">
					<a href="./">ホーム</a>
					<a href="usermanage">ユーザー管理</a>
				</div>
				<div class="header-right">
					<a href="logout">ログアウト</a>
					<c:if test="${ not empty loginUser }">
						<span><c:out value="${ loginUser.name }" /></span>
	            	</c:if>
	            </div>
            </div>
		</div>


			<div class="main-contents">

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages }" var="message">
							<li><c:out value="${message }" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>

			<div class="signup">
			<form action="signup" method="post">
			<div class="signup-form-area">
				<br /><label for="loginId" id="loginId">ログインID<span id ="note">（半角英数字6文字以上20文字以下）</span></label><ul></ul>
				<input type="text" name="loginId" value="${signUpUser.loginId }" class="signup-form" id="user-check"/><br />
				<label for="name">ユーザー名<span id ="note">（10文字以下）</span></label>
				<input type="text" name="name" value="${signUpUser.name }" class="signup-form" /><br />

				<label for="password">パスワード<span id ="note">（半角文字6文字以上20文字以下）</span></label>
				<input name="password" type="password" class="signup-form" /><br />

				<label for="passwordConfirm">パスワード【確認】</label>
				<input type="password" name="passwordConfirm" class="signup-form"><br />

				<label for="brachId">支店</label>
				<select name="branchId" id="branchId">
					<c:forEach items="${branches}" var="branch">
						<c:if test="${signUpUser.branchId == branch.id}">
							<option value="${signUpUser.branchId}" selected>${branch.name}</option>
						</c:if>
						<c:if test="${signUpUser.branchId != branch.id}">
							<option value="${ branch.id }">${branch.name}</option>
						</c:if>
					</c:forEach>
				</select><br />

				<label for="positionId">役職</label>
					<select name="positionId" id="positionId">
						<c:forEach items="${positions}" var="position">
							<c:if test="${signUpUser.positionId == position.id}">
								<option value="${signUpUser.positionId}" selected>${position.name}</option>
							</c:if>
							<c:if test="${signUpUser.positionId != position.id}">
								<option value="${ position.id }">${position.name}</option>
							</c:if>
						</c:forEach>
					</select><br /><br />

				<input type="submit" value="登録" class="button"/>
				</div>
			</form>
			</div><br /><br />
			<div class="copyright">Coryright(c)Uetake Shiho</div>
		</div>
	</body>
</html>