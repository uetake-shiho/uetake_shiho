<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./css/style.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" charset="UTF-8"></script>
		<title>ユーザー管理</title>
		<script type="text/javascript">
			function stop(){
				if(window.confirm('停止してもよろしいですか？')){
					location.href ="http://localhost:8080/Uetake_Shiho/usermanage";
					return true;
				}
				return false;
			}
			function revival(){
				if(window.confirm('復活してもよろしいですか？')){
					location.href ="http://localhost:8080/Uetake_Shiho/usermanage";
					return true;
				}
				return false
			}
		</script>
	</head>
	<body>
		<div class="header">
			<div class="container">
				<div class="header-left">
					<a href="./">ホーム</a>
					<a href="signup">ユーザー登録</a>
				</div>
				<div class="header-right">
					<a href="logout">ログアウト</a>
					<c:if test="${ not empty loginUser }">
						<span><c:out value="${ loginUser.name }" /></span>
	            	</c:if>
	            </div>
            </div>
		</div>

			<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message }" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="manage-table">
			<table border="1">
				<tr>
					<th>ログインID</th>
					<th>ユーザー名</th>
					<th>部署</th>
					<th>役職</th>
					<th>稼動状況</th>
					<th>編集</th>
				</tr>


					<c:forEach items="${users}" var="user">
					<tr>

						<td><c:out value="${user.loginId}" /></td>
						<td><c:out value="${user.name}" /></td>

						<c:forEach items="${branches}" var="branch">
							<c:if test="${ user.branch_id == branch.id }">
								<td><c:out value="${branch.name}" /></td></c:if>
						</c:forEach>

						<c:forEach items="${positions}" var="position">
							<c:if test="${ user.position_id == position.id }">
								<td><c:out value="${position.name}" /></td>
							</c:if>
						</c:forEach>

						<c:if test="${ loginUser.id != user.id }">
							<c:if test="${user.is_stopped == 0}">
								<td align="center">
									<form method="post" action="stop">
										<input type=hidden name=userId value="${user.id}">
										<input type=submit value="停止" onClick="return stop()" class="stop-button">
									</form>
								</td>
							</c:if>

							<c:if test="${user.is_stopped == 1}">
								<td align="center">
									<form method="post" action="revival">
										<input type=hidden name=userId value="${user.id}">
										<input type=submit value="復活" onClick="return revival()" class="revival-button">
									</form>
								</td>
							</c:if>
						</c:if>

						<c:if test="${ loginUser.id == user.id }">
						<td align="center">ログイン中</td>
						</c:if>

						<td align="center"><form method="get" action="settings">
						<input type=hidden name=userId value="${user.id}">
						<input type=submit value="編集" class="settings-button"></form></td>
					</tr>

					</c:forEach>
			</table></div>
			<div class="copyright"> Copyright(c)UetakeShiho</div>
		</div>
	</body>
</html>