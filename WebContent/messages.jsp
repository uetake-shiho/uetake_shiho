<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="./js/ajax.js"></script>
		<title>新規投稿</title>
	</head>
	<body>
		<div class="header">
			<div class="container">
				<div class="header-left">
					<a href="./">ホーム</a>
				</div>
				<div class="header-right">
					<a href="logout">ログアウト</a>
					<c:if test="${ not empty loginUser }">
						<span><c:out value="${ loginUser.name }" /></span>
	            	</c:if>
	            </div>
            </div>
		</div>
			<div class="main-contents">

				<div class="error-area">
					<ul></ul>
				</div>

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message }" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<div class="new-messages">
				<form id="newMessage-form">
				<div class="new-messages-form">
					<label for="subject">件名<span id ="note">（30文字以内）</span></label>
					<input type="text" id="subject" class="subject" value="${subject}"/><br /><br />
					<label for="text">本文<span id ="note">（1000文字以内）</span></label>
					<textarea id="text" cols="100" rows="10" class="text">${text }</textarea><br /><br />
					<label for="category">カテゴリー<span id ="note">（10文字以内）</span></label>
					<input type="text" id="category" class="message-category" value="${ category }"/><br /><br/>

					<button id="newMessage" class="button" type="button">投稿</button>
				</div>
				</form>




			</div>
			<div class="copyright"> Copyright(c)UetakeShiho</div>
		</div>
	</body>
</html>