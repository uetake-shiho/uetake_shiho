<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3//DTD HTML 4.01 transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<title>ユーザー編集</title>
</head>
<body>
	<div class="header">
			<div class="container">
				<div class="header-left">
					<a href="./">ホーム</a>
					<a href="usermanage">ユーザー管理</a>
				</div>
				<div class="header-right">
					<a href="logout">ログアウト</a>
					<c:if test="${ not empty loginUser }">
						<span><c:out value="${ loginUser.name }" /></span>
	            	</c:if>
	            </div>
            </div>
		</div>

		<div class="main-contents">


		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages }" var="message">
						<li><c:out value="${message }" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>
		<div class="settings">
		<form action="settings" method="post">
			<div class="settings-form-area">
			<input name="id" value="${ editUser.id }" id="id" type="hidden" />

			<label for="loginId">ログインID<span id ="note">（半角英数字6文字以上20文字以下）</span></label>
			<input type="text" name="loginId" value="${editUser.loginId }" class="settings-form" /><br />

			<label for="name">ユーザー名<span id ="note">（10文字以下）</span></label>
			<input type="text" name="name" value="${editUser.name }" class="settings-form" /> <br />

			<label for="password">パスワード<span id ="note">（半角文字6文字以上20文字以下）</span></label>
			<input name="password" type="password" class="settings-form" /><br />

			<label for="passwordConfirm">パスワード【確認】</label>
			<input type="password" name="passwordConfirm" class="settings-form"><br>

			<c:if test="${ loginUser.id == editUser.id }">
				<label for="brachId">支店</label>
				<select name="branchId" id="branchId">
					<c:forEach items="${branches}" var="branch">
						<c:if test="${editUser.branchId == branch.id}">
							<option value="${branch.id}" selected>${branch.name}</option>
						</c:if>
					</c:forEach>
				</select><br />

				<label for="positionId">役職</label>
				<select name="positionId" id="positionId">
					<c:forEach items="${positions}" var="position">
						<c:if test="${editUser.positionId == position.id}">
							<option value="${position.id}" selected>${position.name}</option>
						</c:if>
					</c:forEach>
				</select><br />
			</c:if>

			<c:if test="${ loginUser.id != editUser.id }">
				<label for="brachId">支店</label>
				<select name="branchId" id="branchId">
					<c:forEach items="${branches}" var="branch">
						<c:if test="${editUser.branchId == branch.id}">
							<option value="${editUser.branchId}" selected>${branch.name}</option>
						</c:if>
						<c:if test="${editUser.branchId != branch.id}">
							<option value="${ branch.id }">${branch.name}</option>
						</c:if>
					</c:forEach>
				</select><br />

				<label for="positionId">役職</label>
				<select name="positionId" id="positionId">
					<c:forEach items="${positions}" var="position">
						<c:if test="${editUser.positionId == position.id}">
							<option value="${editUser.positionId}" selected>${position.name}</option>
						</c:if>
						<c:if test="${editUser.positionId != position.id}">
							<option value="${ position.id }">${position.name}</option>
						</c:if>
					</c:forEach>
				</select><br />
			</c:if>

			<input type="submit" value="編集" class="button"/> <br/>

			</div>
		</form>
		</div>

		<div class="copyright">Coryright(c)Uetake Shiho</div>
	</div>


</body>
</html>