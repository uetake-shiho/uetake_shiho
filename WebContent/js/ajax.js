$(function(){
	$('button#newMessage').on('click',function(){

		var form = $(this).parents("form#newMessage-form");
		var subject = form.find('#subject').val();
		var text = form.find('#text').val();
		var category = form.find('#category').val();
		var message = { 'subject': subject, 'text': text, 'category': category };

		$(".error-area").each(function(k, v){ $(v).find("ul").empty(); })

		$.ajax({
			dataType: 'json',
			type:'POST',
			url: './newMessage',
			data: { message: JSON.stringify(message) }

		}).done(function(data, textStatus, jqXHR) {
			if(data.is_success == 'true') {
				console.log('success!!');
			} else {

				var errorArea = $(".main-contents").find(".error-area");

				data.errors.forEach(function(v, k) {

				errorArea.find("ul").append("<li>" + v + "</li>");
				});
			}
		}).fail(function(data, textStatus, jqXHR) {

			console.log('error!!');
		});

	});
});

$(function(){
	$('button#comment-button').on('click',function(){

		var form = $(this).parents("form#comment-form-area");
		var messageId = form.find('#messageId').val();
		var text = form.find('#text').val();
		var userName = form.find('#userName').val();
		var comment = { 'messageId': messageId, 'text': text };

		var today = new Date();
		var year = today.getFullYear();
		var month = today.getMonth() + 1;
		var day = today.getDate();
		var hours = today.getHours();
		var minutes = today.getMinutes();
		var seconds = today.getSeconds();
		$(".error-area").each(function(k, v){ $(v).find("ul").empty(); })

		$.ajax({
			dataType: 'json',
			type:'POST',
			url: './comment',
			data: { comment: JSON.stringify(comment) }

		}).done(function(data, textStatus, jqXHR) {
			if(data.is_success == 'true') {
				console.log('success!!');
				var id = data.id;
				$("div.message-list").find("#view_" + messageId).append(
						"<div class='comment'><p class='comment-text'>" + text + "</p>" + "<div id='comment-name'>" +
						userName + "</div>" + "<div id=\"comment-date\">" + year + "/" + month +
						"/" + day + " " + hours + ":" + minutes + ":" + seconds + "</div>" +
						"<form action='deleteComment' method='get'>" +
						"<input type=hidden name=commentId value='" + id + "'>" +
						"<input type=submit value='コメント削除' onClick='return commentsdelete()' class='delete-comment-button'>" +
					"</form>");

				$(".comment-form").each(function(k, v) { $(this).val(""); })
			} else {

				var errorArea = $(".main-contents").find(".error-area");

				data.errors.forEach(function(v, k) {

				errorArea.find("ul").append("<li>" + v + "</li>");
				});
			}
		}).fail(function(data, textStatus, jqXHR) {

			console.log('error!!');
		});

	});
});

$(function(){
	$('input[type="text"]#user-check').keyup(function(){

		var checkId = $(this).val();
		$(".signup-form-area").each(function(k, v){ $(v).find("ul").empty(); })

		if(checkId !=''){
			$.ajax({
				dataType: 'json',
				type:'POST',
				url: './usercheck',
				data: { 'checkId': JSON.stringify(checkId) }

			}).done(function(data, textStatus, jqXHR) {
				if(data.is_success == 'true') {
					console.log('success!!');

				} else {
					data.errors.forEach(function(v, k) {
					$(".signup-form-area").find("ul").append("<li>" + v + "</li>");
					console.log(v);
					});
				}
			}).fail(function(data, textStatus, jqXHR) {
				console.log('error!!');
			});
		}

	});
});
