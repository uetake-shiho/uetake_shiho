<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./css/style.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" charset="UTF-8"></script>
		<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="./js/ajax.js"></script>
		<title>掲示板</title>
        <script type="text/javascript">
			function messagesdelete(){
				if(window.confirm('削除してもよろしいですか？')){
					location.href ="http://localhost:8080/Uetake_Shiho/index.jsp";
					return true;
				}
				return false;
			}
			function commentsdelete(){
				if(window.confirm('削除してもよろしいですか？')){
					location.href ="http://localhost:8080/Uetake_Shiho/index.jsp";
					return true;
				}
				return false
			}
		</script>
    </head>
    <body>
		<div class="header">
			<div class="container">
				<div class="header-left">
					<a href="./">ホーム</a>
					<a href="newMessage">新規投稿</a>
					<c:if test="${ loginUser.branchId == 1 && loginUser.positionId == 1 }">
						<a href="usermanage">ユーザー管理</a>
					</c:if>
				</div>
				<div class="header-right">
					<a href="logout">ログアウト</a>
					<c:if test="${ not empty loginUser }">
						<span><c:out value="${ loginUser.name }" /></span>
	            	</c:if>
	            </div>
            </div>
		</div>

            <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message }" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="error-area">
				<ul></ul>
			</div>

			<form action="./" method="get">
				<div class="retrieval">
				<ul class="category">
					<li><label for="category">カテゴリー</label>
					<input type="text" name="category" value="${category}" id="category" /></li>
					<li><label for="date">日付</label><input type="date" name="minDate" value="${minDate}" ></input>
					<span> ～ </span>
					<input type="date" name="maxDate" value="${maxDate}" ></input></li>
					<li><input type="submit" value="検索" class="retrieval-button"/></li>
				</ul>
				</div>
				</form><br/>

			<c:forEach items="${messages}" var="message">
				<div class="message-list">
					<div class="message">
					<span id="message-subject"><c:out value="${message.subject}" /></span><br />
					<p class="message-text"><c:out value="${message.text}" /></p>
					<span id="message-name">投稿者:<c:out value="${message.name}" /></span><br />
					<span id="message-category">カテゴリー：<c:out value="${message.category}" /></span><br />
					<span id="message-date"><fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></span><br />
					<c:if test="${message.loginId == loginUser.loginId }">
						<form action="deleteMessage" method="get">
							<input type=hidden name=messageId value="${message.id}">
							<input type=submit value="投稿削除" onClick="return messagesdelete()" class="delete-button">
						</form>
					</c:if>
					</div>


					<div id="view_${message.id}"></div>


						<c:forEach items="${comments}" var="comment">
							<c:if test="${message.id == comment.messageId }">
							<div class="comment">
								<p class="comment-text"><c:out value="${comment.text}" /></p>
								<div id="comment-name"><c:out value="${comment.name}" /></div>
								<div id="comment-date"><fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
								<c:if test="${comment.loginId == loginUser.loginId }">
									<form action="deleteComment" method="get">
										<input type=hidden name=commentId value="${comment.id}">
										<input type=submit value="コメント削除" onClick="return commentsdelete()" class="delete-comment-button">
									</form>
								</c:if>
							</div>
							</c:if>
						</c:forEach>



					<div class="comment-form-area">
						<form id="comment-form-area">
							<input type="hidden" id="messageId" value="${message.id }"  />
							<input type="hidden" id="userName" value="${loginUser.name }"  />
							<label for="comment">【500文字以内で入力してください】</label>
		            		<textarea id="text" cols="50" rows="3" class="comment-form"></textarea><br />
		            		<button type="button" id="comment-button" class="comment-button">コメント</button>
						</form>
						<br />
					</div>
				</div><br /><br />
				</c:forEach>
			<div class="copyright"> Copyright(c)UetakeShiho</div>
		</div>
    </body>
</html>